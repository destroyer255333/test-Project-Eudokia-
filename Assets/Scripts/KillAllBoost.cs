﻿using UnityEngine;

public class KillAllBoost : MonoBehaviour
{
    private Enemy[] enemies;

    public void KillAll()
    {
        enemies = FindObjectsOfType<Enemy>();

        for (int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i].gameObject);
        }
    }
}
