﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class Enemy : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] protected HealthBar healthBar;
    [SerializeField] private int health;
    [Space]
    [SerializeField] private float speed = 5f;
    [Space]
    [SerializeField] private UnityEvent OnSpawnEvent;
    [SerializeField] private UnityEvent OnClickEvent;
    [SerializeField] private UnityEvent OnDeadEvent;

    protected NavMeshAgent agent;

    protected SimplePath path = new SimplePath();
    protected Vector3 movePos = new Vector3();
    protected WinLoseGame winLose;

    private AudioManager audioManager;

    public float Speed { get => speed; set => speed = value; }
    public int Health { get => health; set => health = value; }

    private void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
        winLose = FindObjectOfType<WinLoseGame>();
        agent = transform.GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        OnSpawnEvent.Invoke();

        healthBar.SetHealth(Health);
        agent.speed = this.Speed;

        AddPhysicsRaycaster();

        movePos = path.GetPath();
        agent.destination = movePos;
    }

    private void FixedUpdate()
    {
        Move();
    }

    public void Play(AudioClip clip)
    {
        audioManager.PlayGameAudio(clip);
    }

    public abstract void Move();
    public abstract void Click();
    public abstract void Dead();

    public void AddPhysicsRaycaster()
    {
        PhysicsRaycaster physicsRaycaster = GameObject.FindObjectOfType<PhysicsRaycaster>();
        if (physicsRaycaster == null)
        {
            Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClickEvent.Invoke();
        Click();
    }

    private void OnDestroy()
    {
        OnDeadEvent.Invoke();
    }
}
