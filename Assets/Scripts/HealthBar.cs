﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image bar;

    private HealthSystem healthSystem;
    private int health;

    private void Start()
    {
        healthSystem = new HealthSystem(health);
        healthSystem.OnHealthChanged += HealthSystem_OnHealthChanged;
    }

    public void SetHealth(int value)
    {
        health = value;
    }

    public void ApplyDamage(int value)
    {
        healthSystem.Damage(value);
    }

    private void HealthSystem_OnHealthChanged()
    {
        UpdateBar();

        if (healthSystem.IsHealthNull())
        {
            Destroy(this.transform.parent.gameObject);
        }
    }

    private void UpdateBar()
    {
        bar.fillAmount = healthSystem.GetHealthNormalized();
    }
}
