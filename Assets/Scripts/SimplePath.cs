﻿using UnityEngine;
using System.Collections;

public class SimplePath
{
    private Transform objectToPath;
    private readonly RandomPointFind randomPointFind = new RandomPointFind();

    public Vector3 GetPath()
    {
        objectToPath = GameObject.FindGameObjectWithTag("Field").transform;
        return randomPointFind.GetRandomPoint(objectToPath);
    }

    
}

public class RandomPointFind
{
    /// <summary>
    /// Возвращает вектор в определенной области.
    /// </summary>
    /// <param name="_transform">объект в котором нужно возвратить точку</param>
    /// <returns></returns>
    public Vector3 GetRandomPoint(Transform _transform)
    {
        float x = Random.Range(-_transform.localScale.x / 2, _transform.localScale.x / 2);
        float z = Random.Range(-_transform.localScale.z / 2, _transform.localScale.z / 2);

        var point = new Vector3(x, _transform.position.y + 1, z);

        return point;
    }
}
