﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource audioSourceGame;
    [SerializeField] private AudioSource audioSourceMusic;
    [Space]
    [SerializeField] private UnityEvent OnStartGame;

    private void Start()
    {
        OnStartGame.Invoke();
    }

    public void PlayGameAudio(AudioClip clip)
    {
        audioSourceGame.PlayOneShot(clip);
    }

    public void PlayMusicAudio(AudioClip clip)
    {
        audioSourceMusic.PlayOneShot(clip);
    }
}
