﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

public class WinLoseGame : MonoBehaviour
{
    [SerializeField] private int countToLose = 10;
    [SerializeField] private UnityEvent OnLoseGame;
    
    private List<Enemy> enemies = new List<Enemy>();

    public void Add(Enemy _enemy)
    {
        enemies.Add(_enemy);

        CheckLose();
    }

    public void Remove(Enemy _enemy)
    {
        Debug.LogError("remove"); 
        enemies.Remove(_enemy);
    }

    public void CheckLose()
    {
        if (enemies.Count >= countToLose)
        {
            OnLoseGame.Invoke();
        }
    }
}
