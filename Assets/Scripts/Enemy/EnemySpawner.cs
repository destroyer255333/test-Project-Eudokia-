﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> enemiesPrefab;
    [SerializeField] private Transform objectToPlace;
    [SerializeField] private WinLoseGame winLose;
    [Space]
    [SerializeField] private float minSpawnTime;
    [SerializeField] private float maxSpawnTime;

    private float spawnTime;
    private bool isSpawn = true;


    private IncreaseDifficult increaseDifficult;
    private readonly RandomPointFind randomPointFind = new RandomPointFind();

    public float SpawnTime { get => spawnTime; set => spawnTime = value; }
    public bool IsSpawn { get => isSpawn; set => isSpawn = value; }

    private void Awake()
    {
        increaseDifficult = gameObject.GetComponent<IncreaseDifficult>();
    }

    private void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    private IEnumerator SpawnEnemy()
    {
        while (true)
        {
            spawnTime = Random.Range(minSpawnTime, maxSpawnTime);

            if (enemiesPrefab.Count > 0 && this.isSpawn)
            {
                yield return new WaitForSeconds(this.spawnTime);

                GameObject objToSpawn = enemiesPrefab[Random.Range(0, enemiesPrefab.Count)];

                //Spawn enemy in scene
                GameObject enemyObj = Instantiate(objToSpawn, transform);
                enemyObj.transform.position = randomPointFind.GetRandomPoint(objectToPlace);
                Enemy enemy = enemyObj.GetComponent<Enemy>();

                increaseDifficult.IncreaseEnemies(enemy);
                winLose.Add(enemy);

            }

            yield return new WaitForEndOfFrame();
        }
    }
}