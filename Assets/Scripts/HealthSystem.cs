﻿using UnityEngine;
using System.Collections;
using System;

public class HealthSystem
{
    private int health;
    private int healthMax;

    public event Action OnHealthChanged;

    public HealthSystem(int _healthMax)
    {
        this.healthMax = _healthMax;
        health = this.healthMax;
    }

    public void Heal(int value)
    {
        health += value;

        if (health > healthMax)
        {
            health = healthMax;
        }

        OnHealthChanged.Invoke();
    }

    public void Damage(int value)
    {
        health -= value;

        if (health < 0)
        {
            health = 0;
        }

        Debug.LogError("health " + health);
        Debug.LogError("health max " + healthMax);
        OnHealthChanged.Invoke();
    }

    public bool IsHealthNull()
    {
        if (health <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public float GetHealthNormalized()
    {
        return (float)health / healthMax;
    }
}
