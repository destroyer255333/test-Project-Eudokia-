﻿using System.Collections;
using UnityEngine;

public class FreezeSpawnBoost : MonoBehaviour
{
    [SerializeField] private float seconds;
    private EnemySpawner spawner;

    private void Awake()
    {
        spawner = FindObjectOfType<EnemySpawner>();
    }

    public void FreezeSpawnTime()
    {
        StartCoroutine(FreezeSpawnTimeCoroutine());
    }

    IEnumerator FreezeSpawnTimeCoroutine()
    {
        while (spawner.IsSpawn == true)
        {
            spawner.IsSpawn = false;
            Debug.LogError("freeze " + spawner.IsSpawn);
            yield return new WaitForSeconds(seconds);
        }

        spawner.IsSpawn = true;
        Debug.LogError("freeze end " + spawner.IsSpawn);
        yield break;
    }
}
