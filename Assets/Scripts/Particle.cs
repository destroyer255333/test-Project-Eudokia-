﻿using UnityEngine;
public class Particle : MonoBehaviour
{

    private ParticleSystem ps;

    private void Awake()
    {
        ps = gameObject.GetComponent<ParticleSystem>();    
    }

    private void Update()
    {
        if (ps.isStopped)
        {
            Destroy(ps.gameObject);
        }
    }
}
