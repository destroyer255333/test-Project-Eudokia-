﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseDifficult : MonoBehaviour
{

    [SerializeField] private int countToIncrease = 5;
    [Space]
    [SerializeField] private int increaseHealth = 1;
    [SerializeField] private float increaseSpeed = 1f;
    [SerializeField] private float increaseSpawnTime = 1f;

    private EnemySpawner spawner;
    private int count = 0;

    private void Awake()
    {
        spawner = gameObject.GetComponent<EnemySpawner>();
    }

    public void IncreaseEnemies(Enemy _enemy)
    {
        count++;

        if (count >= countToIncrease)
        {
            count = default;

            _enemy.Health += increaseHealth;
            _enemy.Speed += increaseSpeed;

            spawner.SpawnTime -= increaseSpawnTime;
            increaseSpawnTime += increaseSpawnTime;
        }
    }

    public void IncreaseEnemies(List<Enemy> _enemies)
    {
        count++;

        if (count >= countToIncrease)
        {
            count = default;

            for (int i = 0; i < _enemies.Count; i++)
            {
                _enemies[i].Health += increaseHealth;
                _enemies[i].Speed += increaseSpeed;
            }

            spawner.SpawnTime -= increaseSpawnTime;
        }
    }
    
}
