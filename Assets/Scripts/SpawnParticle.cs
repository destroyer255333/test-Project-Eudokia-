﻿using UnityEngine;

public class SpawnParticle : MonoBehaviour
{
    public void Spawn(GameObject particlePrefab)
    {
       Instantiate(particlePrefab, transform.position, Quaternion.identity).GetComponent<ParticleSystem>();
    }
}
