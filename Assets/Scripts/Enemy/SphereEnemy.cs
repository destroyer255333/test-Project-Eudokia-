﻿using UnityEngine;

public class SphereEnemy : Enemy
{
    public override void Click()
    {
        healthBar.ApplyDamage(1);
    }

    public override void Dead()
    {
        winLose.Remove(this);
        Destroy(this.gameObject);
    }

    public override void Move()
    {
        if (Vector3.Distance(transform.position, movePos) < 0.1)
        {
            movePos = path.GetPath();
            agent.destination = movePos;

            Debug.LogError("MOVE POS CUBE " + movePos);
            return;
        }

    }
}
