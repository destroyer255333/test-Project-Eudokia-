﻿
using UnityEngine;

public class CubeEnemy : Enemy
{

    public override void Move()
    {
        if (Vector3.Distance(transform.position, movePos) < 0.5)
        {
            movePos = path.GetPath();
            agent.destination = movePos;
        }
    }

    public override void Click()
    {
        healthBar.ApplyDamage(1);
        Debug.LogError("tap");
    }

    public override void Dead()
    {
        winLose.Remove(this);
        Destroy(this.gameObject);
    }

}
